package com.lokalkart.admin.client;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.binary.Base64;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class Client extends CommonClientBuilder {

	public static void getTokens() throws ClientProtocolException, IOException, ParseException {
		String name = "acme";
		String password = "acmesecret";
		String authString = name + ":" + password;
		System.out.println("auth string: " + authString);
		byte[] authEncBytes = Base64.encodeBase64(authString.getBytes());
		String authStringEnc = new String(authEncBytes);
		System.out.println("Base64 encoded auth string: " + authStringEnc);
		HttpPost httppost;
		httppost = new HttpPost("http://localhost:8080/oauth/token");
		httppost.setHeader("Authorization", "Basic " + authStringEnc); 
		JSONObject jsonObject = httpClientJsonBuilder(httppost);
		// get a String from the JSON object
		String accessToken = (String) jsonObject.get("access_token");
		String tokenType = (String) jsonObject.get("token_type");
		new AuthorizationParam(accessToken, tokenType);
	}
	
	public static JSONObject commonRequest(HttpPost httppost, JSONObject jsonObj)
			throws IOException, ClientProtocolException, ParseException {
		AuthorizationParam.getToken();
		httppost.setHeader("Authorization", AuthorizationParam.getTokenType() + " " + AuthorizationParam.getAccessToken()); 
		httppost.setHeader("Content-Type", "application/json");
		httppost.setHeader("Accept", "application/json");

		CloseableHttpClient httpClient = HttpClients.createDefault();
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("myJSON", jsonObj.toString()));

		StringEntity entity = new StringEntity(jsonObj.toString(),"utf-8");
		httppost.setEntity(entity);

		CloseableHttpResponse httpResponse = httpClient.execute(httppost);
		System.out.println(httpResponse.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();

		httpClient.close();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(response.toString());
		return jsonObject;
	}
	
	public static JSONArray commonRequestForJSONArray(HttpPost httppost, JSONObject jsonObj)
			throws IOException, ClientProtocolException, ParseException {
		AuthorizationParam.getToken();
		httppost.setHeader("Authorization", AuthorizationParam.getTokenType() + " " + AuthorizationParam.getAccessToken()); 
		httppost.setHeader("Content-Type", "application/json");
		httppost.setHeader("Accept", "application/json");

		CloseableHttpClient httpClient = HttpClients.createDefault();
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("myJSON", jsonObj.toString()));

		StringEntity entity = new StringEntity(jsonObj.toString(),"utf-8");
		httppost.setEntity(entity);

		CloseableHttpResponse httpResponse = httpClient.execute(httppost);
		System.out.println(httpResponse.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();

		httpClient.close();
		JSONParser jsonParser = new JSONParser();
		JSONArray jsonObject = (JSONArray) jsonParser.parse(response.toString());
		return jsonObject;
	}
}