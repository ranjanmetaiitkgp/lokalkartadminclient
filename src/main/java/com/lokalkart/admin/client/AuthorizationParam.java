package com.lokalkart.admin.client;


public class AuthorizationParam {

	public AuthorizationParam(String accessToken,String tokenType){
		this.accessToken = accessToken;
		this.tokenType = tokenType;
	}
	
	private static String accessToken;
	public static String getAccessToken() {
		return accessToken;
	}

	public static void setAccessToken(String accessToken) {
		AuthorizationParam.accessToken = accessToken;
	}

	public static String getTokenType() {
		return tokenType;
	}

	public static void setTokenType(String tokenType) {
		AuthorizationParam.tokenType = tokenType;
	}

	private static String tokenType;
	
	public static synchronized AuthorizationParam  getToken(){
		return new AuthorizationParam(accessToken, tokenType);
	}
	
}
