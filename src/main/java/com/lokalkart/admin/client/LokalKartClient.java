package com.lokalkart.admin.client;

import java.io.BufferedReader;

import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Map;

import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClientBuilder;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
@RestController
@Component
public class LokalKartClient extends CommonClientBuilder {
	@RequestMapping(value = "/lokalkartclient", method = RequestMethod.GET)
	public static Object main() throws ClientProtocolException, IOException, ParseException {
		Client.getTokens();
		/*String countryId = getCountryId();
		String stateId = getStateId(countryId);
		String cityId = getCityId(stateId);
		saveLocation(cityId);*/
		return getAllCity();
		
	}
	
	
	public Object processSubmit(Map<String,String> data) throws Exception{
		
		Client.getTokens();
		String countryId = getCountryId((String)data.get("countryName"),(String)data.get("countryCode"));
		String stateId = getStateId((String)data.get("stateName"),(String)data.get("stateCode"),countryId);
		String cityId = getCityId((String)data.get("cityName"),stateId);
		saveLocation((String)data.get("locationName"),cityId);
		return getAllCity();
	}

	private static Object getAllCity() throws ClientProtocolException, IOException, ParseException {
		String url = "http://localhost:8080/city/getAllCity";

		HttpClient client = HttpClientBuilder.create().build();
		HttpGet request = new HttpGet(url);
		
		request.setHeader("Authorization", AuthorizationParam.getTokenType() + " " + AuthorizationParam.getAccessToken()); 
		request.setHeader("Content-Type", "application/json");
		request.setHeader("Accept", "application/json");
		HttpResponse response = client.execute(request);

		System.out.println("Response Code : " 
	                + response.getStatusLine().getStatusCode());

		BufferedReader rd = new BufferedReader(
			new InputStreamReader(response.getEntity().getContent()));

		StringBuffer result = new StringBuffer();
		String line = "";
		while ((line = rd.readLine()) != null) {
			result.append(line);
		}
		
		System.out.println(result);
		
		JSONParser jsonParser = new JSONParser();
		Object obj = jsonParser.parse(result.toString());
		return obj;
	}
}
