package com.lokalkart.admin.client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.message.BasicNameValuePair;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

public class CommonClientBuilder {
	public static JSONObject httpClientJsonBuilder(HttpPost httppost)
			throws UnsupportedEncodingException, IOException, ClientProtocolException, ParseException {
		ArrayList<NameValuePair> postParameters;
		CloseableHttpClient httpClient = HttpClients.createDefault();
		postParameters = new ArrayList<NameValuePair>();
		postParameters.add(new BasicNameValuePair("grant_type", "password"));
		postParameters.add(new BasicNameValuePair("scope", "openid"));
		postParameters.add(new BasicNameValuePair("username", "admin"));
		postParameters.add(new BasicNameValuePair("password", "admin"));

		HttpEntity entity = new UrlEncodedFormEntity(postParameters, "utf-8");
		httppost.setEntity(entity);

		CloseableHttpResponse httpResponse = httpClient.execute(httppost);
		
		BufferedReader reader = new BufferedReader(new InputStreamReader(
		        httpResponse.getEntity().getContent()));

		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
		    response.append(inputLine);
		}
		reader.close();

		httpClient.close();
		
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(response.toString());
		return jsonObject;
	}
	
	public static String getCountryId(String countryName, String countryCode) throws IOException, ClientProtocolException, ParseException {
		HttpPost httppost;
		httppost = new HttpPost("http://localhost:8080/country/saveCountry");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("countryName", countryName);
		jsonObj.put("countryCode", countryCode);

		JSONObject jsonObject = commonRequest(httppost, jsonObj);

		
		String countryId = (String) jsonObject.get("countryId");
		return countryId;
	}
	
	public static String getStateId(String stateName, String stateCode,String countryId) throws IOException, ClientProtocolException, ParseException {
		HttpPost httppost;
		httppost = new HttpPost("http://localhost:8080/state/saveState");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("stateName", stateName);
		jsonObj.put("stateCode", stateCode);
		jsonObj.put("countryId", countryId);

		JSONObject jsonObject = commonRequest(httppost, jsonObj);

		String stateId = (String) jsonObject.get("stateId");
		return stateId;
	}
	
	public static String getCityId(String cityName,String stateId) throws IOException, ClientProtocolException, ParseException {
		HttpPost httppost;
		httppost = new HttpPost("http://localhost:8080/city/saveCity");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("cityName", cityName);
		jsonObj.put("stateId", stateId);

		JSONObject jsonObject = commonRequest(httppost, jsonObj);

		String cityId = (String) jsonObject.get("cityId");
		return cityId;
		
	}
	
	public static void saveLocation(String locationName,String cityId) throws IOException, ClientProtocolException, ParseException {
		HttpPost httppost;
		httppost = new HttpPost("http://localhost:8080/location/saveLocation");

		JSONObject jsonObj = new JSONObject();
		jsonObj.put("locationName",locationName);
		jsonObj.put("cityId", cityId);

		JSONObject jsonObject = commonRequest(httppost, jsonObj);

		/*String cityId = (String) jsonObject.get("cityId");
		return cityId;*/
		
	}
	
	
	

	private static JSONObject commonRequest(HttpPost httppost, JSONObject jsonObj)
			throws IOException, ClientProtocolException, ParseException {
		AuthorizationParam.getToken();
		httppost.setHeader("Authorization", AuthorizationParam.getTokenType() + " " + AuthorizationParam.getAccessToken()); 
		httppost.setHeader("Content-Type", "application/json");
		httppost.setHeader("Accept", "application/json");

		CloseableHttpClient httpClient = HttpClients.createDefault();
		List<NameValuePair> parameters = new ArrayList<NameValuePair>();
		parameters.add(new BasicNameValuePair("myJSON", jsonObj.toString()));

		StringEntity entity = new StringEntity(jsonObj.toString(),"utf-8");
		httppost.setEntity(entity);

		CloseableHttpResponse httpResponse = httpClient.execute(httppost);
		System.out.println(httpResponse.toString());
		BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent()));
		String inputLine;
		StringBuffer response = new StringBuffer();

		while ((inputLine = reader.readLine()) != null) {
			response.append(inputLine);
		}
		reader.close();

		httpClient.close();
		JSONParser jsonParser = new JSONParser();
		JSONObject jsonObject = (JSONObject) jsonParser.parse(response.toString());
		return jsonObject;
	}

}
