package com.lokalkart.admin.bean;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.SessionScoped;
import javax.faces.component.UIViewRoot;
import javax.faces.context.FacesContext;
import javax.imageio.ImageIO;

import org.apache.http.client.methods.HttpPost;
import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.primefaces.context.RequestContext;
import org.primefaces.event.FileUploadEvent;
import org.primefaces.model.UploadedFile;
import org.springframework.stereotype.Component;

import com.lokalkart.admin.client.Client;
import com.lokalkart.admin.model.AppImage;
import com.lokalkart.admin.model.Brand;
import com.lokalkart.admin.model.Product;
import com.lokalkart.admin.model.ProductCategory;
import com.lokalkart.admin.model.ProductSubCategory;

@ManagedBean
@SessionScoped
@Component
public class LokalkartMBean {

	private HttpPost httppost;
	private String productCategorySelected;
	private String productSubCategorySelected;
	private String productSelected;
	private String brandSelected; 
	private int index;
	private String deleteProduct;
	private String editProduct;


	public String getDeleteProduct() {
		return deleteProduct;
	}

	public void setDeleteProduct(String deleteProduct) {
		this.deleteProduct = deleteProduct;
	}

	public String getEditProduct() {
		return editProduct;
	}

	public void setEditProduct(String editProduct) {
		this.editProduct = editProduct;
	}

	public int getIndex() {
		return index;
	}

	public void setIndex(int index) {
		this.index = index;
	}

	private Product product = new Product();
	public Product getProduct() {
		return product;
	}

	public void setProduct(Product product) {
		this.product = product;
	}

	public String getProductSubCategorySelected() {
		return productSubCategorySelected;
	}

	public void setProductSubCategorySelected(String productSubCategorySelected) {
		this.productSubCategorySelected = productSubCategorySelected;
	}

	public String getProductSelected() {
		return productSelected;
	}

	public void setProductSelected(String productSelected) {
		this.productSelected = productSelected;
	}

	private List<ProductCategory> prodCategoryList;
	private List<ProductSubCategory> productSubCategory;
	private List<Product> productList;
	private List<Brand> brandList;

	public List<Brand> getBrandList() {
		for (int i = 0; i < prodCategoryList.size(); i++) {
			ProductCategory category = prodCategoryList.get(i);
			if(productCategorySelected.equals(category.getProductCategory())){
				List<ProductSubCategory> proSubCatList = category.getProductSubCategoryList();
				for (Iterator<ProductSubCategory> iterator = proSubCatList.iterator(); iterator.hasNext();) {
					ProductSubCategory productSubCategory = (ProductSubCategory) iterator.next();
					if(productSubCategorySelected.equals(productSubCategory.getSubCategoryName())){
						setBrandList(productSubCategory.getBrandList());
					}

				}
			}
		}
		return brandList;
	}

	public void setBrandList(List<Brand> brandList) {
		this.brandList = brandList;
	}

	
	public List<Product> getProductList() {
		for (int i = 0; i < prodCategoryList.size(); i++) {
			ProductCategory category = prodCategoryList.get(i);
			if(productCategorySelected.equals(category.getProductCategory())){
				List<ProductSubCategory> proSubCatList = category.getProductSubCategoryList();
				for (Iterator<ProductSubCategory> iterator = proSubCatList.iterator(); iterator.hasNext();) {
					ProductSubCategory productSubCategory = (ProductSubCategory) iterator.next();
					if(productSubCategorySelected.equals(productSubCategory.getSubCategoryName())){
						List<Brand> brandList = productSubCategory.getBrandList();
						for (Iterator<Brand> iterator1 = brandList.iterator(); iterator1.hasNext();) {
							Brand brand = iterator1.next();
							if(brandSelected.equals(brand.getBrandName())){
								setProductList(brand.getProductList());
							}
						}

					}
				}
			}
		}
		return productList;
	}
	
	@PostConstruct
	public void init(){
		fetchProductCategoryList();
	}

	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}

	public String navigateToProductSubCategory(){
		System.out.println(productCategorySelected);
		return "/pages/subcategory.xhtml";
	}
	public String navigateToBrand(){
		System.out.println(brandSelected);
		return "/pages/brand.xhtml";
	}
	
	public String navigateToProductDetails(){
		System.out.println(productSelected);
		for(Product p : productList){
			if(productSelected.equals(p.getProductName())){
				setProduct(p);
			}
		}
		return "/pages/productDetails.xhtml";
	}

	public String navigateToProduct(){
		System.out.println(productCategorySelected);
		return "/pages/product.xhtml";
	}

	@SuppressWarnings("unchecked")
	private void fetchProductCategoryList(){

		try {
			Client.getTokens();
			JSONObject productCateogryJson = new JSONObject();
			JSONObject productSubCateogryJson = new JSONObject();
			JSONObject brandJson = new JSONObject();
			JSONObject productJson = new JSONObject();
			JSONObject appImageJson = new JSONObject();
			JSONArray ja = new JSONArray();
			JSONArray imgJsonArray = new JSONArray();
			
			/*appImageJson = new JSONObject();
			appImageJson.put("imageUrl", "http://localhost:8081/lokalkartimages/ProductCategory/SubCategory/Brand/Product/Household-Icon.png");
			JSONObject appImageSizeJson = new JSONObject();
			appImageSizeJson.put(, );
			//appImageJson.put("imageSize", appImageSizeJson);
			ja.add(appImageJson);
			
			httppost = new HttpPost("http://localhost:8080/product/saveProduct");
			productJson.put("productName", "Nike Running Shoes");
			productJson.put("productImages", ja);
			productJson = Client.commonRequest(httppost, productJson);
			
			ja = new JSONArray();
			appImageJson = new JSONObject();
			appImageJson.put("imageUrl", "http://localhost:8081/lokalkartimages/ProductCategory/SubCategory/Brand/Product/Grocery.png");
			JSONObject appImageSizeJson = new JSONObject();
			appImageSizeJson.put(, );
			//appImageJson.put("imageSize", appImageSizeJson);
			imgJsonArray.add(appImageJson);

			httppost = new HttpPost("http://localhost:8080/brand/saveBrand");
			ja.add(productJson);
			brandJson.put("brandName", "Nike");
			brandJson.put("products", ja);
			brandJson.put("productImages", imgJsonArray);
			brandJson = Client.commonRequest(httppost, brandJson);
			
			ja = new JSONArray();
			imgJsonArray = new JSONArray();
			appImageJson = new JSONObject();
			appImageJson.put("imageUrl", "http://localhost:8081/lokalkartimages/ProductCategory/SubCategory/mobile_shop.png");
			JSONObject appImageSizeJson = new JSONObject();
			appImageSizeJson.put(, );
			//appImageJson.put("imageSize", appImageSizeJson);
			imgJsonArray.add(appImageJson);
			
			httppost = new HttpPost("http://localhost:8080/productSubCategory/saveProductSubCategory");
			ja.add(brandJson);
			productSubCateogryJson.put("subCategoryName", "Sports Shoe");
			productSubCateogryJson.put("brand", ja);
			productSubCateogryJson.put("productSubCategoryImage", imgJsonArray);
			productSubCateogryJson = Client.commonRequest(httppost, productSubCateogryJson);

			appImageJson = new JSONObject();
			ja = new JSONArray();
			imgJsonArray = new JSONArray();
			appImageJson.put("imageUrl", "http://localhost:8081/lokalkartimages/ProductCategory/Electrical_Shop.png");
			JSONObject appImageSizeJson = new JSONObject();
			appImageSizeJson.put(, );
		//	appImageJson.put("imageSize", appImageSizeJson);
			imgJsonArray.add(appImageJson);
			
			httppost = new HttpPost("http://localhost:8080/productCategory/saveProductCategory");

			ja.add(productSubCateogryJson);
			productCateogryJson.put("categoryName", "Men");
			productCateogryJson.put("subCategories", ja);
			productCateogryJson.put("productCategoryImage", imgJsonArray);

			productCateogryJson = Client.commonRequest(httppost, productCateogryJson);*/

			/*httppost = new HttpPost("http://localhost:8080/productCategory/updateProductCategory");
		ja = new JSONArray();
		ja.add(productSubCateogryJson);
		productCateogryJson.put("categoryName", "Electronics");
		productCateogryJson.put("subCategories", ja);
		productCateogryJson.put("catId", "5664307456bcdb9412a38c41");

		productCateogryJson = Client.commonRequest(httppost, productCateogryJson);*/



			List<ProductCategory> productCategoryList = new ArrayList<ProductCategory>();
			httppost = new HttpPost("http://localhost:8080/productCategory/getAllProductCategory");

			JSONArray jsonArray =  Client.commonRequestForJSONArray(httppost, productJson);
			for (int i = 0; i < jsonArray.size(); i++) {
				ProductCategory productCategory = new ProductCategory();
				JSONObject jsonObjectTemp = (JSONObject) jsonArray.get(i);
				String catName = (String) jsonObjectTemp.get("categoryName");
				String catId = (String) jsonObjectTemp.get("catId");
				productCategory.setProductCategoryId(catId);
				productCategory.setProductCategory(catName);
				
				JSONArray jsonArrayForPCImages = (JSONArray) jsonObjectTemp.get("productCategoryImage");
				for (int pc = 0; pc < jsonArrayForPCImages.size(); pc++) {
					JSONObject jsonObjectTempForProdCat = (JSONObject) jsonArrayForPCImages.get(pc);
					AppImage appImg = new AppImage();
					appImg.setImageSize(null);
					appImg.setImageUrl((String) jsonObjectTempForProdCat.get("imageUrl"));
					productCategory.getProductImages().add(appImg);
				}
				
				
				
				JSONArray jsonArrayForSubCat = (JSONArray) jsonObjectTemp.get("subCategories");
				for (int j = 0; j < jsonArrayForSubCat.size(); j++) {
					ProductSubCategory productSubCategory = new ProductSubCategory();
					JSONObject jsonObjectTempForSubCat = (JSONObject) jsonArrayForSubCat.get(j);
					String subCatName = (String) jsonObjectTempForSubCat.get("subCategoryName");
					String subCatId = (String) jsonObjectTempForSubCat.get("subCatId");
					productSubCategory.setSubCategoryName(subCatName);
					productSubCategory.setSubCatId(subCatId);
					
					JSONArray jsonArrayForSPCImages = (JSONArray) jsonObjectTempForSubCat.get("productSubCategoryImage");
					for (int pc = 0; pc < jsonArrayForSPCImages.size(); pc++) {
						JSONObject jsonObjectTempForSubCatImg = (JSONObject) jsonArrayForSPCImages.get(pc);
						AppImage appImg = new AppImage();
						appImg.setImageSize(null);
						appImg.setImageUrl((String) jsonObjectTempForSubCatImg.get("imageUrl"));
						productSubCategory.getProductImages().add(appImg);
					}
					
					
					JSONArray jsonArrayForBrand = (JSONArray) jsonObjectTempForSubCat.get("brand");
					for (int k = 0; k < jsonArrayForBrand.size(); k++) {
						JSONObject jsonObjectTempForBrand = (JSONObject) jsonArrayForBrand.get(k);
						Brand brand = new Brand();
						brand.setBrandId((String)jsonObjectTempForBrand.get("brandId"));
						brand.setBrandName((String)jsonObjectTempForBrand.get("brandName"));
						
						JSONArray jsonArrayForBrandImages = (JSONArray) jsonObjectTempForBrand.get("productImages");
						for (int pc = 0; pc < jsonArrayForBrandImages.size(); pc++) {
							JSONObject jsonObjectTempForBrandImg = (JSONObject) jsonArrayForSPCImages.get(pc);
							AppImage appImg = new AppImage();
							appImg.setImageSize(null);
							appImg.setImageUrl((String) jsonObjectTempForBrandImg.get("imageUrl"));
							brand.getProductImages().add(appImg);
						}
						
						JSONArray jsonArrayForProduct = (JSONArray) jsonObjectTempForBrand.get("products");
						for (int n = 0; n < jsonArrayForProduct.size(); n++) {
							Product prod = new Product();
							JSONObject jsonObjectTempForProduct = (JSONObject) jsonArrayForProduct.get(k);
							String prodName = (String) jsonObjectTempForProduct.get("productName");
							String prodId = (String) jsonObjectTempForProduct.get("productId");
							prod.setProductId(prodId);
							prod.setProductName(prodName);
							
							JSONArray jsonArrayForProdImages = (JSONArray) jsonObjectTempForProduct.get("productImages");
							for (int pc = 0; pc < jsonArrayForProdImages.size(); pc++) {
								JSONObject jsonObjectTempForProdImg = (JSONObject) jsonArrayForProdImages.get(pc);
								AppImage appImg = new AppImage();
								appImg.setImageSize(null);
								appImg.setImageUrl((String) jsonObjectTempForProdImg.get("imageUrl"));
								prod.getProductImages().add(appImg);
							}
							
							brand.getProductList().add(prod);
							productSubCategory.getBrandList().add(brand);
						}
						productCategory.getProductSubCategoryList().add(productSubCategory);
					}
					productCategoryList.add(productCategory);
				}
			}
			setProdCategoryList(productCategoryList);
		} 
		catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String getBrandSelected() {
		return brandSelected;
	}

	public void setBrandSelected(String brandSelected) {
		this.brandSelected = brandSelected;
	}

	public List<ProductSubCategory> getProductSubCategory(){
		List<ProductSubCategory> productSubCategoryList = new LinkedList<ProductSubCategory>();
		for (int i = 0; i < prodCategoryList.size(); i++) {
			ProductCategory category = prodCategoryList.get(i);
			if(productCategorySelected.equals(category.getProductCategory())){
				List<ProductSubCategory> proSubCatList = category.getProductSubCategoryList();
				for(ProductSubCategory subCategory :proSubCatList){
					ProductSubCategory prodSubCat = new ProductSubCategory();
					prodSubCat.setSubCategoryName(subCategory.getSubCategoryName());
					for (AppImage appImage : subCategory.getProductImages()) {
						AppImage appImg = new AppImage();
						appImg.setImageUrl(appImage.getImageUrl());
						prodSubCat.getProductImages().add(appImage);
					}
					productSubCategoryList.add(prodSubCat);
				}
				break;
			}
		}
		productSubCategory = productSubCategoryList;
		return productSubCategory;
	}




	public String getProductCategorySelected() {
		return productCategorySelected;
	}
	public void setProductCategorySelected(String productCategorySelected) {
		this.productCategorySelected = productCategorySelected;
	}

	public List<ProductCategory> getProdCategoryList() {
		return prodCategoryList;
	}

	public void setProdCategoryList(List<ProductCategory> prodCategoryList) {
		this.prodCategoryList = prodCategoryList;
	}
	private String image;
	int i =0;

	public String getImage() {

		if(i==0){
			i++;
			image= "mobile_shop.png";
			return image;
		}else{
			image= "Home-Appliance.png";
			return image;
		}

	}

	public void setImage(String image) {
		this.image = image;
	}
	
	public void editProduct(){
		System.out.println(index);
		System.out.println(editProduct);
		
	}
public void deleteProduct(){
	System.out.println(index);
	System.out.println(deleteProduct);
	UIViewRoot view = FacesContext.getCurrentInstance().getViewRoot();
	}

public void handleFileUpload(FileUploadEvent event) throws IOException {
	InputStream inputStream = null;
	OutputStream outputStream = new FileOutputStream("E://img.jpg");
	try{
		
		URL url = new URL("http://localhost:8081/lokalkartimages/ProductCategory/Electrical_Shop.png");
		
		BufferedImage img = ImageIO.read(url);
		File file = new File(url.getFile());
		
		if(file.delete()){
			System.out.println(file.getName() + " is deleted!");
		}else{
			System.out.println("Delete operation is failed.");
		}
		FacesMessage message = new FacesMessage("Succesful", event.getFile().getFileName() + " is uploaded.");
		FacesContext.getCurrentInstance().addMessage(null, message);
		RequestContext context = RequestContext.getCurrentInstance();
		context.execute("PF('dlg2').hide();");
		UploadedFile uploadedFile = event.getFile();
		inputStream = uploadedFile.getInputstream();
		int read = 0;
		byte[] bytes = new byte[1024];

		while ((read = inputStream.read(bytes)) != -1) {
			outputStream.write(bytes, 0, read);
		}

		System.out.println("Done!");

	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (inputStream != null) {
			try {
				inputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		if (outputStream != null) {
			try {
				// outputStream.flush();
				outputStream.close();
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
	}
}
}
