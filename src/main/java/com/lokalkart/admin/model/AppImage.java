package com.lokalkart.admin.model;

import java.io.Serializable;

/**
 * 
 * @author Rohit Ranjan
 *
 */
public class AppImage implements Serializable{

	
	/**
	 * 
	 */
	private static final long serialVersionUID = 7743812216448320171L;


	private String id;
	
	private String imageUrl;
	
	private ProductImageSize imageSize;
	
	public String getImageUrl() {
		return imageUrl;
	}
	public void setImageUrl(String imageUrl) {
		this.imageUrl = imageUrl;
	}
	public ProductImageSize getImageSize() {
		return imageSize;
	}
	public void setImageSize(ProductImageSize imageSize) {
		this.imageSize = imageSize;
	}
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}

}
