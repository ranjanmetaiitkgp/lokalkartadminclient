package com.lokalkart.admin.model;

import java.util.LinkedList;
import java.util.List;

public class Brand {

	private String brandId;
	public String getBrandId() {
		return brandId;
	}
	public void setBrandId(String brandId) {
		this.brandId = brandId;
	}
	public String getBrandName() {
		return brandName;
	}
	public void setBrandName(String brandName) {
		this.brandName = brandName;
	}
	public List<Product> getProductList() {
		if(null==productList){
			productList = new LinkedList<Product>();
		}
		return productList;
	}
	public void setProductList(List<Product> productList) {
		this.productList = productList;
	}
	private String brandName;
	private List<Product> productList;
	private List<AppImage> productImages;


	public List<AppImage> getProductImages() {
		if(null==productImages){
			productImages = new LinkedList<AppImage>();
		}
		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}
}
