package com.lokalkart.admin.model;

import java.util.LinkedList;
import java.util.List;

public class ProductCategory {

	private String productCategory;
	private String productCategoryId;
	public String getProductCategoryId() {
		return productCategoryId;
	}
	public void setProductCategoryId(String productCategoryId) {
		this.productCategoryId = productCategoryId;
	}
	public String getProductCategory() {
		return productCategory;
	}
	public void setProductCategory(String productCategory) {
		this.productCategory = productCategory;
	}

	private List<ProductSubCategory> productSubCategoryList;
	public List<ProductSubCategory> getProductSubCategoryList() {
		if(null == productSubCategoryList){
			productSubCategoryList = new LinkedList<ProductSubCategory>();
		}
		return productSubCategoryList;
	}
	public void setProductSubCategoryList(List<ProductSubCategory> productSubCategoryList) {
		this.productSubCategoryList = productSubCategoryList;
	}
	private List<AppImage> productImages;


	public List<AppImage> getProductImages() {
		if(null == productImages){
			productImages = new LinkedList<AppImage>();
		}

		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}
}
