package com.lokalkart.admin.model;

import java.util.LinkedList;
import java.util.List;


public class ProductSubCategory {

	private String subCatId;


	private String subCategoryName;

	private List<Brand> brandList;


	public String getSubCategoryName() {
		return subCategoryName;
	}

	private List<AppImage> productImages;


	public List<AppImage> getProductImages() {
		if(null==productImages){
			productImages = new LinkedList<AppImage>();
		}
		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}

	public List<Brand> getBrandList() {
		if(null==brandList){
			brandList = new LinkedList<Brand>();
		}
		return brandList;
	}

	public void setBrandList(List<Brand> brandList) {
		this.brandList = brandList;
	}

	public void setSubCategoryName(String subCategoryName) {
		this.subCategoryName = subCategoryName;
	}

	public String getSubCatId() {
		return subCatId;
	}

	public void setSubCatId(String subCatId) {
		this.subCatId = subCatId;
	}



}
