package com.lokalkart.admin.model;

/**
 * 
 * @author Rohit Ranjan
 *
 */
public enum ProductImageSize {

	SMALL, LARGE, NORMAL, MEDIUM;
}
