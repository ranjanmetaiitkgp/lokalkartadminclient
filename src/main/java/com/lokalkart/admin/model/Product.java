package com.lokalkart.admin.model;

import java.util.LinkedList;
import java.util.List;


public class Product {

	
	private String productId;
	
	
	private String itemCode;
	
	
	private String productName;
	
	private String productDescription;
	
	//@TODO change double type to BigInteger
	private Double maxRetailPrice;
	private Double discountPercentage;
	private Double sellingPrice;
	private List<AppImage> productImages;

	
	public List<AppImage> getProductImages() {
		if(null==productImages){
			productImages = new LinkedList<AppImage>();
		}
		return productImages;
	}

	public void setProductImages(List<AppImage> productImages) {
		this.productImages = productImages;
	}

	private String deliveryTime;
	
	private Double deliveryCharge;
	
	
	private List<String> keyFeatures;
	
	private String productSubCategoryId;
	
	public String getProductSubCategoryId() {
		return productSubCategoryId;
	}

	public void setProductSubCategoryId(String productSubCategoryId) {
		this.productSubCategoryId = productSubCategoryId;
	}

	public String getProductId() {
		return productId;
	}

	public void setProductId(String productId) {
		this.productId = productId;
	}

	public String getItemCode() {
		return itemCode;
	}

	public void setItemCode(String itemCode) {
		this.itemCode = itemCode;
	}

	public String getProductName() {
		return productName;
	}

	public void setProductName(String productName) {
		this.productName = productName;
	}

	public String getProductDescription() {
		return productDescription;
	}

	public void setProductDescription(String productDescription) {
		this.productDescription = productDescription;
	}

	public Double getMaxRetailPrice() {
		return maxRetailPrice;
	}

	public void setMaxRetailPrice(Double maxRetailPrice) {
		this.maxRetailPrice = maxRetailPrice;
	}

	public Double getDiscountPercentage() {
		return discountPercentage;
	}

	public void setDiscountPercentage(Double discountPercentage) {
		this.discountPercentage = discountPercentage;
	}

	public Double getSellingPrice() {
		return sellingPrice;
	}

	public void setSellingPrice(Double sellingPrice) {
		this.sellingPrice = sellingPrice;
	}

	public String getDeliveryTime() {
		return deliveryTime;
	}

	public void setDeliveryTime(String deliveryTime) {
		this.deliveryTime = deliveryTime;
	}

	public Double getDeliveryCharge() {
		return deliveryCharge;
	}

	public void setDeliveryCharge(Double deliveryCharge) {
		this.deliveryCharge = deliveryCharge;
	}


	public List<String> getKeyFeatures() {
		return keyFeatures;
	}

	public void setKeyFeatures(List<String> keyFeatures) {
		this.keyFeatures = keyFeatures;
	}

	//@TODO Product Review and Ratings

}
