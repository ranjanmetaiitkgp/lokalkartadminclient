package com.lokalkart.admin;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class LokalKartAdminClientApplication {

    public static void main(String[] args) {
        SpringApplication.run(LokalKartAdminClientApplication.class, args);
      
    }
    
   
}
